FROM python:3.7-slim-stretch


RUN apt-get update && apt-get install -y --no-install-recommends build-essential

COPY requirements.txt /requirements.txt
RUN pip install -r requirements.txt

WORKDIR /
RUN mkdir resources
COPY resources/word2vec_vacancies.model /resources
COPY resources/word2vec_vacancies.model.trainables.syn1neg.npy /resources
COPY resources/word2vec_vacancies.model.wv.vectors.npy /resources

ENV APP_HOME /app
RUN mkdir $APP_HOME
COPY $APP_HOME/ $APP_HOME
WORKDIR $APP_HOME

EXPOSE 8080

ENTRYPOINT ["python"]
CMD ["app.py"]
